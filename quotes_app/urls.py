"""Quotes App Urls"""
from django.urls import path

from . import views

app_name = 'quotes_app'

urlpatterns = [
   path('', views.index, name='index'),
   path('delete_quote/<quote_id>', views.delete_quote, name='delete_quote'),
]
