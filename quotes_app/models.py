"""Quotes models"""
from django.db import models


class Quote(models.Model):
    """A quote table"""
    text = models.CharField(max_length=200)
