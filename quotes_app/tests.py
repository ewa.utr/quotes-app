"""Unit testing"""
from django.test import TestCase
from .models import Quote

# Create your tests here.
class QuotesTest(TestCase):
    """Test for quotes"""
    @classmethod
    def setUpTestData(cls):
        """Setting up the database data"""
        #create a quote
        new_quote = Quote.objects.create(text='This is a test quote.')
        new_quote.save()

    def test_quote_content(self):
        """Checking if the data got added"""
        quote = Quote.objects.get(id=1)
        expected_text = f'{quote.text}'
        self.assertEqual(expected_text, 'This is a test quote.')
