"""Apps file"""
from django.apps import AppConfig


class QuotesAppConfig(AppConfig):
    """Quotes config"""
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'quotes_app'
