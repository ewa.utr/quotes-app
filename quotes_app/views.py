"""Views"""
from django.shortcuts import get_object_or_404, render
from .models import Quote


def index(request):
    """Showing index page"""

    if request.method == "POST":
        text = request.POST["text"]
        quote = Quote()
        quote.text = text
        quote.save()

    quotes = Quote.objects.all()
    context = {
      'quotes' : quotes
    }
    return render(request, 'quotes_app/index.html', context)

def delete_quote(request, quote_id):
    """Deleting quote"""

    quote = get_object_or_404(Quote, pk=quote_id)
    quote.delete()

    quotes = Quote.objects.all()
    context = {
      'quotes' : quotes
    }
    return render(request, 'quotes_app/index.html', context)
