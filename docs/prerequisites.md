# Prerequisites

In order to collaborate on this project, please clone this repository.

    $ git clone https://gitlab.com/ewa.utr/quotes-app.git

Once the project is cloned down to your local machine, open it in your editor, e.g.

    $ cd quotes-app && code .

To run the project locally, ensure you have Docker or Docker Desktop installed on your machine. If you are not familiar with Docker you can read their documentation here.
