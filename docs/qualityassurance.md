# Software Quality Assurance

## Coding standards

All developers collaborating on the Quotes app must follow the coding standards defined by Django, for collaboration on Django projects. Please read through the following documentation before you start work on the project.
https://docs.djangoproject.com/en/dev/internals/contributing/writing-code/coding-style/

## CI/CD pipeline

Please follow the rules for continuous integration and continuous delivery for this project.
All developers must commit their changes once a single feature is done and working. Do not push non-working code to the repository.
All commits must have a comment that explains the newly added code and what purpose it serves.
At the end of every work day we will deliver the new features to production.
Pipeline:
A pipeline will be triggered every time a push is made to any branch. The pipeline will perform a unit test of your code followed by linting. For linting we are using Pylint-django.
If you do not pass the pipeline, please read the error messages provided and fix the bugs/errors. Once this is done you should be able to perform a new commit and push code to the branch.

## Test usage

We are using a Python standard library for unit testing. The test.py file will be run once the pipeline is triggered and will check if a quote will be created correctly without any errors.
For version 2 of the application we will add more relevant tests. The tests will be described and explained in this section and a notice will be added at the top of this documentation, once they are implemented.
Linting
All code will be analyzed with the help of a linting tool. For this project are we using the plugin pylint-django. The plugin will analyze the stylistic parts of your code based on the Django coding standards. If you do not get a high score, please read the feedback provided by pylint-django and fix the stylistic errors.

## Linting

All code will be analyzed with the help of a linting tool. For this project are we using the plugin pylint-django. The plugin will analyze the stylistic parts of your code based on the Django coding standards. If you do not get a high score, please read the feedback provided by pylint-django and fix the stylistic errors.
