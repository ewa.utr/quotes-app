# The Quotes App

This is a app created using Django and PostgreSQL running with Docker. It has sensible defaults for security, scaling, and workflow.

## Quick start

Running the application requires locally installed Docker (https://docs.docker.com/installation/).

Clone this repository:

    $ mkdir my-clone
    $ cd my-clone/
    $ git clone https://gitlab.com/ewa.utr/quotes-app.git

Run the project in Docker's virtual enviroment:

    $ docker-compose up
