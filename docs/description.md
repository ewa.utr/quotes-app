# The Quotes App

## Project Description

Quotes-app is an application written in the python framework Django. The application serves the purposes of giving users the ability to create, read and delete quotes. The quotes’ data are stored in the relational database, postgresQL.

## Project Features

### Creating a quote

The Quotes App allows the user to create a new quote record in the database by submittng a form on the index page. That action triggers the index view with a POST request and sends the new quote data to the database.

### Deleting a quote

When hovering on a single quote, a button appears. Upon click, the specific quote holding the button gets permanently deleted from the database. The 'x' button holds a url that links to the delete_quote view and passes the quote_id argument.
