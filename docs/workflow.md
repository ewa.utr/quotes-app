# Workflow

Every time you are working on a new feature for the project make sure that you create a new branch.

    $ git branch <new_branch_name>

Every time a change is made and the code is working, make sure to add and commit your changes to your branch.

    $ git add .
    $ git commit -m “<A descriptive comment about the feature>”

Once you code is committed, push the code to your branch

    $ git push

If it is the first time you push to your branch, make sure to set the upstream branch in order to make it work

    $ git push --set-upstream <remote> <branch>

Make a merge request to the main branch.
