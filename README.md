# The Quotes App

This is a app created using Django and PostgreSQL running with Docker. It has sensible defaults for security, scaling, and workflow.

## Quick start

Running the application requires locally installed Docker (https://docs.docker.com/installation/).

Clone this repository:

    $ mkdir my-clone
    $ cd my-clone/
    $ git clone https://gitlab.com/ewa.utr/quotes-app.git

Run the project in Docker's virtual enviroment:

    $ docker-compose up

## Walkthrough

Once the Docker container is finished building, the application will be available on `http://0.0.0.0:8080/quotes/`.

### Creating a quote

The Quotes App allows the user to create a new quote record in the database by submittng a form on the index page. That action triggers the index view with a POST request and sends the new quote data to the database.

### Deleting a quote

When hovering on a single quote, a button appears. Upon click, the specific quote holding the button gets permanently deleted from the database. The 'x' button holds a url that links to the delete_quote view and passes the quote_id argument.

## Testing

The testing in the application includes measuring the test coverage by using the [covarage](https://coverage.readthedocs.io/en/coverage-5.5/) and [django_nose](https://django-testing-docs.readthedocs.io/en/latest/coverage.html) libraries.

In order to run tests use the command:

    $ docker-compose run web python manage.py test
